package main

import (
	"flag"
	"fmt"
	"html/template"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	textTmpl "text/template"

	"gopkg.in/yaml.v3"
)

type ResumeData struct {
	Personal struct {
		Name      string `yaml:"name"`
		Birthdate string `yaml:"birthdate"`
		Country   string `yaml:"country"`
		Region    string `yaml:"region"`
		Timezone  string `yaml:"timezone"`
	}
	Job struct {
		Title        string `yaml:"title"`
		Organization string `yaml:"organization"`
	}
	Contact struct {
		Email    string   `yaml:"email"`
		Phone    string   `yaml:"phone"`
		LinkedIn string   `yaml:"linkedin"`
		Websites []string `yaml:"websites"`
	}
	About struct {
		Objective    string `yaml:"objective"`
		Greeting     string `yaml:"greeting"`
		Experience   string `yaml:"experience"`
		Technical    string `yaml:"technical"`
		NonTechnical string `yaml:"nontechnical"`
		Other        string `yaml:"other"`
	}
	Hardskills []string `yaml:"hardskills"`
	Education  []struct {
		Institution string `yaml:"institution"`
		DegreeType  string `yaml:"degree-type"`
		Title       string `yaml:"title"`
		StartDate   string `yaml:"start-date"`
		EndDate     string `yaml:"end-date"`
	} `yaml:"education"`
	Certifications []struct {
		Name string `yaml:"name"`
		Src  string `yaml:"src"`
		Link string `yaml:"link"`
	}
	WorkHistory []struct {
		Company     string `yaml:"company"`
		Job         string `yaml:"job"`
		StartDate   string `yaml:"start-date"`
		EndDate     string `yaml:"end-date"`
		Description string `yaml:"description"`
		Results     string `yaml:"results"`
	} `yaml:"work-history"`
	Projects []struct {
		Name        string `yaml:"name"`
		Img         string `yaml:"img"`
		Description string `yaml:"description"`
		Repo        string `yaml:"repo"`
		View        string `yaml:"view"`
	}
	Languages      []string `yaml:"languages"`
	OtherInterests []string `yaml:"other-interests"`
	Styles         template.CSS
}

type GroupedFiles struct {
	InfoFile      string
	TemplateFiles []string
}

var (
	stylePath     string
	language      string
	templatesPath string
	outputPath    string
	infoPath      string
)

func readInfo(filename string, stylePath string) *ResumeData {
	bData, err := os.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	resume := &ResumeData{}
	err = yaml.Unmarshal(bData, resume)
	if err != nil {
		panic(err)
	}

	if len(stylePath) > 0 {
		resolvedStylePath, _ := filepath.Abs(stylePath)
		styles, err := os.ReadFile(resolvedStylePath)
		if err != nil {
			panic(err)
		}
		resume.Styles = template.CSS(styles)
	}

	return resume
}

func fillTemplate(templatePath string, outPath string, resume *ResumeData) {
	var (
		tmpl         *template.Template
		textTemplate *textTmpl.Template
		err          error
	)

	if strings.Contains(templatePath, ".html") {
		tmpl, err = template.New(filepath.Base(templatePath)).ParseFiles(templatePath)

	} else {
		textTemplate, err = textTmpl.New(filepath.Base(templatePath)).ParseFiles(templatePath)
	}
	if err != nil {
		panic(err)
	}

	file, err := os.Create(outPath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	if tmpl != nil {
		err = tmpl.Execute(file, resume)
	}

	if textTemplate != nil {
		err = textTemplate.Execute(file, resume)
	}

	if err != nil {
		panic(err)
	}
}

func resolvePaths() (rTmplPath, rOutPath, rInfoPath string) {
	rTmplPath, _ = filepath.Abs(templatesPath)
	rOutPath, _ = filepath.Abs(outputPath)
	rInfoPath, _ = filepath.Abs(infoPath)
	return rTmplPath, rOutPath, rInfoPath
}

func getFilesInFolder(dirPath string, language string) []string {
	dir, err := os.Open(dirPath)
	if err != nil {
		panic(err)
	}
	defer dir.Close()

	files, err := dir.ReadDir(0)
	if err != nil {
		panic(err)
	}

	var filteredFiles []string
	for _, v := range files {
		if !v.IsDir() && strings.Contains(v.Name(), language) {
			filteredFiles = append(filteredFiles, fmt.Sprintf("%s/%s", dirPath, v.Name()))
		}
	}

	return filteredFiles
}

func detectLanguages(dirPath string) (languages []string) {
	dir, err := os.Open(dirPath)
	if err != nil {
		panic(err)
	}
	defer dir.Close()

	files, err := dir.ReadDir(0)
	if err != nil {
		panic(err)
	}

	r := regexp.MustCompile(`\.[a-zA-Z]{2}-[a-zA-Z]{2}\.`)
	for _, file := range files {
		if r.MatchString(file.Name()) {
			lang := strings.Split(file.Name(), ".")[1]
			languages = append(languages, lang)
		}
	}

	return languages
}

func appendGroup(groups []GroupedFiles, lang string, infoPath string, templatesPath string) []GroupedFiles {
	newGroup := GroupedFiles{
		InfoFile:      getFilesInFolder(infoPath, lang)[0],
		TemplateFiles: getFilesInFolder(templatesPath, lang),
	}
	return append(groups, newGroup)
}

func groupFilesByLanguage() (groups []GroupedFiles) {
	if language == "" {
		for _, lang := range detectLanguages(infoPath) {
			groups = appendGroup(groups, lang, infoPath, templatesPath)
		}
		return groups
	}

	groups = appendGroup(groups, language, infoPath, templatesPath)
	return groups
}

func main() {
	flag.StringVar(&stylePath, "s", "", "Path to the css styles file")
	flag.StringVar(&language, "l", "", "Select language to use, files should be named <filename>.<language_code>.<extension>, example format: 'en-US'")
	flag.StringVar(&templatesPath, "t", ".", "Path to the folder with all resume templates, defaults to current folder")
	flag.StringVar(&outputPath, "o", ".", "Output path, defaults to current folder")
	flag.StringVar(&infoPath, "i", ".", "Path to the folder with all info files, defaults to current folder")
	flag.Parse()

	templatesPath, outputPath, infoPath = resolvePaths()

	for _, group := range groupFilesByLanguage() {
		resume := readInfo(group.InfoFile, stylePath)
		for _, templateFile := range group.TemplateFiles {
			filename := strings.Split(templateFile, "/")
			newFilename := strings.Replace(filename[len(filename)-1], ".gtpl", "", 1)
			outFile := fmt.Sprintf("%s/%s", outputPath, newFilename)
			fillTemplate(templateFile, outFile, resume)
		}
	}
}
