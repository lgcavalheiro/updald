package main

import (
	"os"
	"path/filepath"
	"slices"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type MainTestSuite struct {
	suite.Suite
	testFile string
}

func (s *MainTestSuite) SetupSuite() {
	s.testFile = "output/resume.en-US.md"
}

func (s *MainTestSuite) TearDownTest() {
	language = ""
	infoPath = ""
	templatesPath = ""
	outputPath = ""

	if _, err := os.Stat(s.testFile); err == nil {
		os.Remove(s.testFile)
	}
}

func (s *MainTestSuite) TestResolvePaths() {
	expected, _ := filepath.Abs(".")
	rTmplPath, rOutPath, rInfoPath := resolvePaths()
	assert.Equal(s.T(), expected, rTmplPath)
	assert.Equal(s.T(), expected, rOutPath)
	assert.Equal(s.T(), expected, rInfoPath)
}

func (s *MainTestSuite) TestGroupFilesByLanguage() {
	language = "en-US"
	infoPath = "fixtures/infos"
	templatesPath = "fixtures/templates"

	groups := groupFilesByLanguage()

	assert.Equal(s.T(), 1, len(groups))
	assert.Equal(s.T(), "fixtures/infos/info.en-US.yaml", groups[0].InfoFile)
	assert.True(s.T(), slices.Contains(groups[0].TemplateFiles, "fixtures/templates/resume.en-US.gtpl.md"))
}

func (s *MainTestSuite) TestAppendGroup() {
	groups := []GroupedFiles{}
	expectedGroup := GroupedFiles{
		InfoFile:      "fixtures/infos/info.en-US.yaml",
		TemplateFiles: []string{"fixtures/templates/resume.en-US.gtpl.md"},
	}

	groups = appendGroup(groups, "en-US", "fixtures/infos", "fixtures/templates")

	assert.Equal(s.T(), expectedGroup, groups[0])
}

func (s *MainTestSuite) TestDetectLanguages() {
	language = "en-US"
	dirPath, _ := filepath.Abs("fixtures/infos")

	langs := detectLanguages(dirPath)

	assert.True(s.T(), slices.Contains(langs, language))
}

func (s *MainTestSuite) TestReadInfo() {
	infoFile, _ := filepath.Abs("fixtures/infos/info.en-US.yaml")

	info := readInfo(infoFile, "")

	assert.Equal(s.T(), "John Doe", info.Personal.Name)
	assert.Empty(s.T(), info.Styles)
}

func (s *MainTestSuite) TestFillTemplate() {
	templatePath, _ := filepath.Abs("fixtures/templates/resume.en-US.gtpl.md")
	outPath, _ := filepath.Abs(s.testFile)
	infoFile, _ := filepath.Abs("fixtures/infos/info.en-US.yaml")
	resume := readInfo(infoFile, "")

	fillTemplate(templatePath, outPath, resume)

	expectedFile, err := os.Stat(s.testFile)
	if err != nil {
		assert.Fail(s.T(), "File not found")
	}

	assert.Equal(s.T(), "resume.en-US.md", expectedFile.Name())
}

func (s *MainTestSuite) TestMain() {
	os.Args = []string{"test", "-l", "en-US", "-i", "fixtures/infos", "-t", "fixtures/templates", "-o", "output"}

	main()

	expectedFile, err := os.Stat(s.testFile)
	if err != nil {
		assert.Fail(s.T(), "File not found")
	}

	assert.Equal(s.T(), "resume.en-US.md", expectedFile.Name())
}

func (s *MainTestSuite) TestGroupFilesByLanguageBlankLanguage() {
	language = ""
	infoPath = "fixtures/infos"
	templatesPath = "fixtures/templates"

	groups := groupFilesByLanguage()

	assert.Equal(s.T(), 1, len(groups))
	assert.Equal(s.T(), "fixtures/infos/info.en-US.yaml", groups[0].InfoFile)
	assert.True(s.T(), slices.Contains(groups[0].TemplateFiles, "fixtures/templates/resume.en-US.gtpl.md"))
}

func (s *MainTestSuite) TestReadInfoWithStyles() {
	stylePath, _ := filepath.Abs("fixtures/templates/styles.css")
	infoFile, _ := filepath.Abs("fixtures/infos/info.en-US.yaml")

	info := readInfo(infoFile, stylePath)

	assert.Equal(s.T(), "John Doe", info.Personal.Name)
	assert.NotEmpty(s.T(), info.Styles)
}

func (s *MainTestSuite) TestReadInfoPanicsIfNoInfoFile() {
	infoFile, _ := filepath.Abs("invalid-info-file.yaml")

	assert.Panics(s.T(), func() { readInfo(infoFile, "") }, "readInfo did not panic")
}

func (s *MainTestSuite) TestReadInfoPanicsIfInfoFileBroken() {
	infoFile, _ := filepath.Abs("fixtures/infos/broken/broken-info.en-US.yaml")

	assert.Panics(s.T(), func() { readInfo(infoFile, "") }, "readInfo did not panic")
}

func (s *MainTestSuite) TestReadInfoPanicsIfStylesPathIsInvalid() {
	stylePath, _ := filepath.Abs("invalid-styles.css")
	infoFile, _ := filepath.Abs("fixtures/infos/info.en-US.yaml")

	assert.Panics(s.T(), func() { readInfo(infoFile, stylePath) }, "readInfo did not panic")
}

func (s *MainTestSuite) TestFillTemplatePanicsIfTemplatePathInvalid() {
	templatePath, _ := filepath.Abs("invalid-resume.en-US.gtpl.md")
	outPath, _ := filepath.Abs(s.testFile)
	infoFile, _ := filepath.Abs("fixtures/infos/info.en-US.yaml")
	resume := readInfo(infoFile, "")

	assert.Panics(s.T(), func() { fillTemplate(templatePath, outPath, resume) }, "fillTemplate did not panic")
}

func (s *MainTestSuite) TestFillTemplatePanicsIfOutPathInvalid() {
	templatePath, _ := filepath.Abs("fixtures/templates/resume.en-US.gtpl.md")
	outPath, _ := filepath.Abs("~/should-panic-here")
	infoFile, _ := filepath.Abs("fixtures/infos/info.en-US.yaml")
	resume := readInfo(infoFile, "")

	assert.Panics(s.T(), func() { fillTemplate(templatePath, outPath, resume) }, "fillTemplate did not panic")
}

func (s *MainTestSuite) TestFillTemplatePanicsIfTemplateIsBroken() {
	templatePath, _ := filepath.Abs("fixtures/templates/broken/broken-template.en-US.gtpl.txt")
	outPath, _ := filepath.Abs(s.testFile)
	infoFile, _ := filepath.Abs("fixtures/infos/info.en-US.yaml")
	resume := readInfo(infoFile, "")

	assert.Panics(s.T(), func() { fillTemplate(templatePath, outPath, resume) }, "fillTemplate did not panic")
}

func (s *MainTestSuite) TestGetFilesInFolderPanicsIfDirPathInvalid() {
	assert.Panics(s.T(), func() { getFilesInFolder("invalid-dir", "") }, "getFilesInFolder did not panic")
}

func (s *MainTestSuite) TestDetectLanguagesPanicsIfDirPathInvalid() {
	assert.Panics(s.T(), func() { detectLanguages("invalid-dir-path") }, "detectLanguages did not panic")
}

func TestMainTestSuite(t *testing.T) {
	suite.Run(t, new(MainTestSuite))
}
