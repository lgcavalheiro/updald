{{ .Personal.Name }}
===================

----
{{ with .Personal }}
> {{ $.Job.Title }} - {{ .Region }}, {{ .Country }} ({{ .Timezone }})   
{{ end }}
{{ with .Contact }}
> <{{ .Email }}> {{ if .Phone }}• {{ .Phone }}{{ end }}  
> [{{ .LinkedIn }}]({{ .LinkedIn }})  
{{ range $website := .Websites }}
> [{{ $website }}]({{ $website }})  
{{ end }}
{{ end }}

----

Summary
------------

{{ .About.Greeting }}   
{{ .About.Objective }}   
{{ .About.Experience }}   
{{ .About.Technical }}   
{{ .About.NonTechnical }}   
{{ .About.Other }}   

Education
------------

{{ range $edu := .Education }}
* **{{ $edu.DegreeType }}, {{ $edu.Title }}** - {{ $edu.Institution }} | {{ $edu.StartDate }}-{{ $edu.EndDate }}
{{ end }}

Work Experience
------------

{{ range $work := .WorkHistory }}
**{{ $work.Job }} - {{ $work.Company }} ({{ $work.StartDate }} - {{ $work.EndDate }})**  

{{ $work.Description }}  
{{ $work.Results }}  
{{ end }}

Certifications
------------

{{ range $cert := .Certifications }}
* [{{ $cert.Name }}]({{ $cert.Link }})   
{{ end }}

Projects
------------

{{ range $proj := .Projects }}
{{ $proj.Name }}
:   {{ $proj.Description }}  

    {{ if .Img }}![{{ $proj.Img }}]({{ $proj.Img }}){{ end }}

    {{ if .Repo }}* [{{ $proj.Repo }}]({{ $proj.Repo}}){{ end }}
    {{ if .View }}* [{{ $proj.View }}]({{ $proj.View }}){{ end }}
{{ end }}

Skills
------------

{{ range $skill := .Hardskills }}
* {{ $skill }}
{{ end }}

Languages
------------

{{ range $lang := .Languages }}
* {{ $lang }}
{{ end  }}

Other interests
------------
{{ range $interest := .OtherInterests }}
* {{ $interest }}
{{ end }}
